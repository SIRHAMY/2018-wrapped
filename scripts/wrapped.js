

// scheme
// * on img load (page load rn), go over elements, setting to appropriate colors
// * have them be editable
// 

// * foreground elements
// * bg elements
// * all editable except img
// * 

// tweakers
let foregroundColor = "#bf3e38";
let backgroundColor = "black";
let metaColor = "white";

// All elementsById

let imageNodeId = "wrapper-content-table__image-cell";
let categoryOneNodeId = "wrapper-content-table__category-one-cell";
let categoryTwoNodeId = "wrapper-content-table__category-two-cell";
let accomplishmentNodeId = "wrapper-content-table__accomplishment-cell";

let bodyNodeId = "body";

let hamyLogoNodeId = "hamy-logo";
let projectNameNodeId = "project-name";

let foregroundElementIds = [
    imageNodeId,
    categoryOneNodeId,
    categoryTwoNodeId,
    accomplishmentNodeId
];

let backgroundElementIds = [
    bodyNodeId
];

let metaElementIds = [
    hamyLogoNodeId,
    projectNameNodeId
];

// Actions

function imageLoaded() {
    foregroundElementIds.forEach(
        elementId => {
            setForegroundColorForNodeId(elementId, foregroundColor);
            setContentEditableForNodeId(
                elementId,
                /* shouldBeEditable */ true
            );
        });

    backgroundElementIds.forEach(
        elementId => setBackgroundColorForNodeId(elementId, backgroundColor)
    );

    metaElementIds.forEach(
        elementId => setForegroundColorForNodeId(elementId, metaColor)
    );
}

// utility functions

function setForegroundColorForNodeId(
    elementId,
    color
) {
    let node = document.getElementById(elementId);
    node.style.color = color;

    // node.style = node.style + "color:" + color + ";";
}

function setBackgroundColorForNodeId(
    elementId,
    color
) {
    let node = document.getElementById(elementId);
    node.style.backgroundColor = color;
    // node.style = node.style + "background-color:" + color + ";";
}

function setContentEditableForNodeId(
    elementId,
    shouldBeEditable
) {
    let node = document.getElementById(elementId);
    node.contentEditable = shouldBeEditable;
}

// Main - hamy: change

imageLoaded();